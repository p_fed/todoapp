package com.zerozero.controller;

import com.zerozero.domain.Task;
import com.zerozero.domain.TaskPriority;
import com.zerozero.domain.TaskState;
import com.zerozero.domain.User;
import com.zerozero.service.TaskPriorityService;
import com.zerozero.service.TaskService;
import com.zerozero.service.TaskStateService;
import com.zerozero.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Frost on 24.09.2017.
 */
@Controller
@RequestMapping("/")
@SessionAttributes({"priorities", "states"})
public class TaskController {
    @Autowired
    TaskPriorityService taskPriorityService;

    @Autowired
    TaskStateService taskStateService;

    @Autowired
    TaskService taskService;

    @Autowired
    UserService userService;

    @ModelAttribute("priorities")
    public List<TaskPriority> initializeTaskPriorities() {
        return taskPriorityService.findAll();
    }

    @ModelAttribute("states")
    public List<TaskState> initializeTaskStates() {
        return taskStateService.findAll();
    }

    @RequestMapping(value = {"/", "/tasks"}, method = RequestMethod.GET)
    public String listTasks(ModelMap model) {

        User user = userService.findByUsername(getPrincipal());
        List<Task> tasks = null;

        if (user != null) {
            tasks = taskService.findByUser(user);
        }

        model.addAttribute("tasks", tasks);
        model.addAttribute("loggedinuser", getPrincipal());
        return "taskslist";
    }

    @RequestMapping(value = {"/tasks/add"}, method = RequestMethod.GET)
    public String addTask(ModelMap model) {
        Task task = new Task();
        model.addAttribute("task", task);
        model.addAttribute("edit", false);
        model.addAttribute("loggedinuser", getPrincipal());
        return "editTask";
    }


    @RequestMapping(value = {"/tasks/edit/{taskId}"}, method = RequestMethod.GET)
    public String editTask(@PathVariable String taskId, ModelMap model) {
        Long id = Long.valueOf(taskId);
        Task task = null;
        if (id != null) {
            task = taskService.findById(id);
        }
        model.addAttribute("loggedinuser", getPrincipal());
        if (!isUserCanModifyTask(task)) {
            return "accessDenied";
        }
        model.addAttribute("task", task);
        model.addAttribute("edit", true);
        return "editTask";
    }

    @RequestMapping(value = "/tasks/save", method = RequestMethod.POST)
    public String saveTask(@Valid Task task, BindingResult result,
                           ModelMap model) {

        if (result.hasErrors()) {
            return "editTask";
        }

        model.addAttribute("loggedinuser", getPrincipal());

        User user = task.getUser();
        if (user != null) {
            if (!isUserCanModifyTask(task)) {
                return "accessDenied";
            }
        }

        user = userService.findByUsername(getPrincipal());
        task.setUser(user);

        if (task.getId() != null) {
            taskService.updateTask(task);
        } else {
            taskService.saveTask(task);
        }

        model.addAttribute("success", "Task " + task.getTitle() + " has been saved successfully");
        return "taskSaveSuccess";
    }

    @RequestMapping(value = {"/tasks/delete/{taskId}"}, method = RequestMethod.GET)
    public String deleteUser(@PathVariable String taskId, ModelMap model) {
        Long id = Long.valueOf(taskId);

        Task task = taskService.findById(id);


        User user = task.getUser();
        if (user != null) {
            if (!isUserCanModifyTask(task)) {
                model.addAttribute("loggedinuser", getPrincipal());
                return "accessDenied";
            }
        }
        taskService.deleteTaskById(id);
        return "redirect:/tasks";
    }

    private boolean isUserCanModifyTask(Task task) {
        if (getPrincipal().equals(task.getUser().getUsername())) {
            return true;
        }
        return false;
    }


    private String getPrincipal() {
        String userName;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
}
