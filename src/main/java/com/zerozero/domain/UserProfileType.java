package com.zerozero.domain;

import java.io.Serializable;

/**
 * Created by Frost on 20.09.2017.
 */
public enum UserProfileType implements Serializable {
    USER("USER"),
    ADMIN("ADMIN");

    String userProfileType;

    private UserProfileType(String userProfileType){
        this.userProfileType = userProfileType;
    }

    public String getUserProfileType(){
        return userProfileType;
    }

}
