package com.zerozero.domain;

import java.io.Serializable;

/**
 * Created by Frost on 24.09.2017.
 */
public enum TaskStateType implements Serializable {
    DONE("DONE"),
    UNPROCESSED("UNPROCESSED"),
    OVERDUE("OVERDUE");

    String taskState;

    private TaskStateType(String taskState){
        this.taskState = taskState;
    }

    public String getTaskState() {
        return taskState;
    }
}
