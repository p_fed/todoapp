package com.zerozero.domain;

import java.io.Serializable;

/**
 * Created by Frost on 24.09.2017.
 */
public enum TaskPriorityType implements Serializable {
    LOW("LOW"),
    NORMAL("NORMAL"),
    HIGH("HIGH");

    String taskPriority;

    private TaskPriorityType(String taskPriority){
        this.taskPriority = taskPriority;
    }

    public String getTaskPriority() {
        return taskPriority;
    }
}
