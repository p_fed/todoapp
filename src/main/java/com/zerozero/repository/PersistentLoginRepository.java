package com.zerozero.repository;

import com.zerozero.domain.PersistentLogin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by Frost on 20.09.2017.
 */
public interface PersistentLoginRepository extends JpaRepository<PersistentLogin, String> {
    Optional<PersistentLogin> findByUsername(String username);
}
