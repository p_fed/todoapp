package com.zerozero.repository;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Frost on 24.09.2017.
 */
public interface TaskPriorityRepository extends JpaRepository<com.zerozero.domain.TaskPriority, Long> {
}
