package com.zerozero.repository;

import com.zerozero.domain.PersistentLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

/**
 * Created by Frost on 20.09.2017.
 */
@Repository("persistentTokenRepository")
@Transactional
public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {
    @Autowired
    PersistentLoginRepository repository;

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        PersistentLogin persistentLogin = new PersistentLogin();
        persistentLogin.setUsername(token.getUsername());
        persistentLogin.setSeries(token.getSeries());
        persistentLogin.setToken(token.getTokenValue());
        persistentLogin.setLastUsed(token.getDate());
        repository.save(persistentLogin);
    }

    @Override
    public void updateToken(String series, String token, Date lastUsed) {
        Optional<PersistentLogin> loginOptional = repository.findById(series);

        if (loginOptional.isPresent()) {
            PersistentLogin persistentLogin = loginOptional.get();

            persistentLogin.setToken(token);
            persistentLogin.setLastUsed(lastUsed);
            repository.save(persistentLogin);
        }
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String series) {
        Optional<PersistentLogin> loginOptional = repository.findById(series);

        if (loginOptional.isPresent()) {
            PersistentLogin persistentLogin = loginOptional.get();

            String username = persistentLogin.getUsername();
            String token = persistentLogin.getToken();
            Date lastUsed = persistentLogin.getLastUsed();
            return new PersistentRememberMeToken(username, series, token, lastUsed);
        }
        return null;
    }

    @Override
    public void removeUserTokens(String username) {
        Optional<PersistentLogin> loginOptional = repository.findByUsername(username);
        if (loginOptional.isPresent()) {
            repository.delete(loginOptional.get());
        }
    }
}
