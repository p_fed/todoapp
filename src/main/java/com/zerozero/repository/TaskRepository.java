package com.zerozero.repository;

import com.zerozero.domain.Task;
import com.zerozero.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

/**
 * Created by Frost on 24.09.2017.
 */
public interface TaskRepository extends JpaRepository<Task, Long> , QuerydslPredicateExecutor<Task> {
    List<Task> findByUser(User user);

    List<Task> findByState(String state);

    List<Task> findByPriority(String priority);

}
