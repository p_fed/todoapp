package com.zerozero.repository;

import com.zerozero.domain.TaskState;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Frost on 24.09.2017.
 */
public interface TaskStateRepository extends JpaRepository<TaskState, Long> {
}
