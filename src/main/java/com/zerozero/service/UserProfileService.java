package com.zerozero.service;

import com.zerozero.domain.UserProfile;

import java.util.List;

/**
 * Created by Frost on 20.09.2017.
 */
public interface UserProfileService {
    UserProfile findById(Long id);

    UserProfile findByType(String type);

    List<UserProfile> findAll();
}
