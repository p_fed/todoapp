package com.zerozero.service;

import com.zerozero.domain.UserProfile;
import com.zerozero.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Created by Frost on 20.09.2017.
 */
@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {
    @Autowired
    UserProfileRepository profileRepository;

    @Override
    public UserProfile findById(Long id) {
        Optional<UserProfile> profileOptional = profileRepository.findById(id);
        try {
            return profileOptional.get();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public UserProfile findByType(String type) {
        Optional<UserProfile> profileOptional = profileRepository.findByType(type);
        try {
            return profileOptional.get();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    @Override
    public List<UserProfile> findAll() {
        return profileRepository.findAll();
    }
}
