package com.zerozero.service;

import com.zerozero.domain.User;

import java.util.List;

/**
 * Created by Frost on 20.09.2017.
 */
public interface UserService {
    Boolean exists(Long id);

    User findById(Long id);

    User findByUsername(String username);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserByUsername(String username);

    List<User> findAllUsers();

    boolean isUserUsernameUnique(Long id, String username);
}
