package com.zerozero.service;

import com.zerozero.domain.TaskState;

import java.util.List;

/**
 * Created by Frost on 24.09.2017.
 */
public interface TaskStateService {
    List<TaskState> findAll();
}
