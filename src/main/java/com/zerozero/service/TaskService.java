package com.zerozero.service;

import com.zerozero.domain.Task;
import com.zerozero.domain.User;

import java.util.List;

/**
 * Created by Frost on 24.09.2017.
 */
public interface TaskService {
    Task findById(Long id);

    List<Task> findByUser(User user);

    List<Task> findByState(String state);

    List<Task> findByPriority(String priority);

    void saveTask(Task task);

    void updateTask(Task task);

    void deleteTaskById(Long id);
}
