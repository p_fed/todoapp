package com.zerozero.service;

import com.zerozero.domain.TaskPriority;
import com.zerozero.repository.TaskPriorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Frost on 24.09.2017.
 */
@Service("taskPriorityService")
@Transactional
public class TaskPriorityServiceImpl implements TaskPriorityService{

    @Autowired
    TaskPriorityRepository taskPriorityRepository;

    @Override
    public List<TaskPriority> findAll() {
        return taskPriorityRepository.findAll();
    }
}
