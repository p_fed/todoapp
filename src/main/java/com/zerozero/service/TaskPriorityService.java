package com.zerozero.service;

import com.zerozero.domain.TaskPriority;

import java.util.List;

/**
 * Created by Frost on 24.09.2017.
 */
public interface TaskPriorityService {
    List<TaskPriority> findAll();
}
