package com.zerozero.service;

import com.zerozero.domain.TaskState;
import com.zerozero.repository.TaskStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Frost on 24.09.2017.
 */
@Service("taskStateService")
@Transactional
public class TaskStateServiceImpl implements TaskStateService {

    @Autowired
    TaskStateRepository taskStateRepository;

    @Override
    public List<TaskState> findAll() {
        return taskStateRepository.findAll();
    }
}
