package com.zerozero.service;

import com.querydsl.jpa.impl.JPAQuery;
import com.zerozero.domain.QTask;
import com.zerozero.domain.QUser;
import com.zerozero.domain.Task;
import com.zerozero.domain.User;
import com.zerozero.repository.TaskRepository;
import com.zerozero.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by Frost on 24.09.2017.
 */
@Service("taskService")
@Transactional
public class TaskServiceImpl implements TaskService {
    @Autowired
    TaskRepository taskRepository;

    @Override
    public Task findById(Long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            return taskOptional.get();
        }
        return null;
    }

    @Override
    public List<Task> findByUser(User user) {
        return taskRepository.findByUser(user);
    }

    @Override
    public List<Task> findByState(String state) {
        return taskRepository.findByState(state);
    }

    @Override
    public List<Task> findByPriority(String priority) {
        return taskRepository.findByPriority(priority);
    }

    @Override
    public void saveTask(Task task) {
        taskRepository.save(task);
    }

    @Override
    public void updateTask(Task task) {
        Optional<Task> taskOptional = taskRepository.findById(task.getId());
        if (taskOptional.isPresent()) {
            Task entity = taskOptional.get();
            entity.setDescription(task.getDescription());
            entity.setDueDate(task.getDueDate());
            entity.setLabel(task.getLabel());
            entity.setPriority(task.getPriority());
            entity.setState(task.getState());
            entity.setTitle(task.getTitle());
        }
    }

    @Override
    public void deleteTaskById(Long id) {
        Task task = findById(id);
        if (task != null) {
            taskRepository.delete(task);
        }
    }
}
