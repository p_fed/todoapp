package com.zerozero.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Frost on 21.09.2017.
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {}
