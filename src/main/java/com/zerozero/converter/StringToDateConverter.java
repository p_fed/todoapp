package com.zerozero.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Frost on 22.09.2017.
 */
@Component
public class StringToDateConverter implements Converter<String, Date> {
    public static final String PATTERN = "dd.MM.yyyy HH:mm";

    public Date convert(String date) {
        try {
            return new SimpleDateFormat(PATTERN).parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
}