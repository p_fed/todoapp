package com.zerozero.converter;

import com.zerozero.domain.UserProfile;
import com.zerozero.service.UserProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by Frost on 22.09.2017.
 */
@Component
public class RoleToUserProfileConverter implements Converter<Object, UserProfile> {
    @Autowired
    UserProfileService userProfileService;

    public UserProfile convert(Object element) {
        Long id = Long.valueOf((String) element);
        UserProfile profile = userProfileService.findById(id);
        return profile;
    }

}